﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using t1_kellyn.Db.Mapping;
using t1_kellyn.Models;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace t1_kellyn.Db
{
    public class conexion : DbContext
    {
        public Microsoft.EntityFrameworkCore.DbSet<Blog> Posts { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server = DESKTOP-M7R6V40\\KELLYN; Database = T1; Trusted_Connection = True; MultipleActiveResultSets = true");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BlogMap());

        }


    }


}
