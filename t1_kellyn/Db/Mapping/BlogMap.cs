﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using t1_kellyn.Models;

namespace t1_kellyn.Db.Mapping
{
    public class BlogMap : IEntityTypeConfiguration<Blog>
    {
        public void Configure(EntityTypeBuilder<Blog> builder)
        {
            builder.ToTable("Blog");
            builder.HasKey(o => o.Id);

            builder.HasMany("Post")
                //.WithMany()
                .HasForeignKey(o => o.PostId);

        }
    }
}
