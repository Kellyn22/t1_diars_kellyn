﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace t1_kellyn.Models
{
    public class Blog
    {
        public int Id { get; set; }
        public string Titulo { get; set; }

        public List<Post> Posts { get; set; }
    }
}
