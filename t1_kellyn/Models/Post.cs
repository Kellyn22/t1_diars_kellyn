﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace t1_kellyn.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Autor { get; set; }

        public int PostId { get; set; }

        public Blog Blog { get; set; }
    }
}
